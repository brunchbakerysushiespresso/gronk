package main

import (
	"context"
	"fmt"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"repo.vffnz.com/RCR-Automation/controllers"
)

func main() {
	client, err := mongo.Connect(context.TODO(), options.Client().ApplyURI("mongodb://localhost:27017"))
	if err != nil {
		panic(err)
	}

	r := httprouter.New()
	controller := controllers.NewController(client)

	r.GET("/posts", controller.GetPosts)
	r.POST("/createpost", controller.CreatePost)
	r.DELETE("/deleteposts", controller.DeletePosts)
	http.ListenAndServe(":3000", r)

	fmt.Println(client)
}
