# RCR-Automation Form API

Simple API built with Go

## Prerequisites

### MongoDB with Docker

```
docker run --name some-mongo -p 27017:27017 -d mongo
```

### Go Dependencies

```
go get go.mongodb.org/mongo-driver/mongo
go get go.mongodb.org/mongo-driver/bson
go get github.com/julienschmidt/httprouter
```

### Run it!

```
go run main.go
```