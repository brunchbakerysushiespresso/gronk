package controllers

import (
	"context"
	"encoding/json"
	"fmt"
	"log"
	"net/http"

	"github.com/julienschmidt/httprouter"
	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/mongo"
)

type Controller struct {
	client *mongo.Client
}

func NewController(c *mongo.Client) *Controller {
	return &Controller{
		client: c,
	}
}

func (c *Controller) GetPosts(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	coll := c.client.Database("postsdb").Collection("posts")

	query := bson.M{}
	cursor, err := coll.Find(context.TODO(), query)
	if err != nil {
		log.Fatal(err)
	}

	results := []bson.M{}

	if err := cursor.All(context.TODO(), &results); err != nil {
		log.Fatal(err)
	}

	w.WriteHeader(http.StatusOK)
	w.Header().Add("Content-Type", "application/json")
	json.NewEncoder(w).Encode(results)
}

func (c *Controller) CreatePost(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	coll := c.client.Database("postsdb").Collection("posts")

	var post bson.M

	if err := json.NewDecoder(r.Body).Decode(&post); err != nil {
		fmt.Println(err)
	}

	_, err := coll.InsertOne(context.TODO(), post)
	if err != nil {
		fmt.Println(err)
	}
	w.WriteHeader(http.StatusOK)
}

func (c *Controller) DeletePosts(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	coll := c.client.Database("postsdb").Collection("posts")

	query := bson.M{}

	_, err := coll.DeleteMany(context.TODO(), query)
	if err != nil {
		fmt.Println(err)
	}
	w.WriteHeader(http.StatusOK)
}
